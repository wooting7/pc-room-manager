# pc방관리 API
***
소개: pc방 관리를 위한 API 입니다
***
#### * 목차 *
```
1. 언어
2. 기능
3. API
```
#### 1. 언어
```
JAVA 16
SpringBoot 2.7.2
```
#### 2. 기능
```
* pc
  -  pc정보 등록
  -  pc이용내역 등록
  -  pc이용내역 조회
  -  pc이용내역 수정  
```

#### 3. API

>* 이미지
> 
>![pc-room-manager](./images/pc-room-manager.png)
