package com.syw.pcroommanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcRoomManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PcRoomManagerApplication.class, args);
    }

}
