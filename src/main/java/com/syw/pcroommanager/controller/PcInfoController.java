package com.syw.pcroommanager.controller;

import com.syw.pcroommanager.entity.PcInfo;
import com.syw.pcroommanager.model.*;
import com.syw.pcroommanager.service.PcInfoService;
import com.syw.pcroommanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "C R U D")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/pc-info")
public class PcInfoController {

    private final PcInfoService pcInfoService;


    @ApiOperation(value = "pc정보 등록하기")
    @PostMapping("/new/pc_info")
    public CommonResult setPcInfo (@RequestBody @Valid PcInfoRequest request) {
        pcInfoService.setPcInfo(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "pc이용내역 등록하기")
    @PostMapping("/new/pc_order-detail")
    public CommonResult setPcOrderDetail (@RequestBody @Valid PcOrderDetailRequest request) {
        PcInfo pcInfo = pcInfoService.getPnInfoNumber(request.getPcId());
        pcInfoService.setPcOrderDetail(pcInfo, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "pc이용내역 수정하기")
    @PutMapping("/pc_order-detail/{id}")
    public CommonResult putMachine(@PathVariable long id, PcOrderDetailUpdateRequest request) {
        pcInfoService.putPcOrderDetail(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "이용내역 확인하기")
    @GetMapping("/serch")
    public ListResult<PcOrderDetailItem> getPcOrderDetails () {
        return ResponseService.getListResult(pcInfoService.getPcOrderDetails(), true);
    }
}
