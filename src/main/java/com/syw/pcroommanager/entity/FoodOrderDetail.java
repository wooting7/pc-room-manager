package com.syw.pcroommanager.entity;

import com.syw.pcroommanager.enums.FoodOrderState;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FoodOrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcId", nullable = false)
    private PcInfo pcInfo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "foodId", nullable = false)
    private FoodManu FoodManu;

    @Column(nullable = false)
    private Integer foodAmount;

    @Column(nullable = false)
    private Double foodPriceTotal;

    @Column(nullable = false, length = 100)
    private String customerSay;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 10)
    private FoodOrderState foodOrderState;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

}
