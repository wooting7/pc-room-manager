package com.syw.pcroommanager.entity;

import com.syw.pcroommanager.intefaces.CommonModelBuilder;
import com.syw.pcroommanager.model.PcInfoRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String pcName;

    @Column(nullable = false, length = 50)
    private String pcSpec;

    @Column(nullable = false)
    private Double pcPrice;

    @Column(nullable = false)
    private LocalDate dateBuy;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column
    private LocalDateTime dateUpdate;

    private PcInfo(PcInfoBuilder builder) {
        this.pcName = builder.pcName;
        this.pcSpec = builder.pcSpec;
        this.pcPrice = builder.pcPrice;
        this.dateBuy = builder.dateBuy;
        this.dateCreate = builder.dateCreate;

    }

    public static class PcInfoBuilder implements CommonModelBuilder<PcInfo> {
        private final String pcName;
        private final String pcSpec;
        private final Double pcPrice;
        private final LocalDate dateBuy;
        private final LocalDateTime dateCreate;


        public PcInfoBuilder(PcInfoRequest request) {
            this.pcName = request.getPcName();
            this.pcSpec = request.getPcSpec();
            this.pcPrice = request.getPcPrice();
            this.dateBuy = request.getDateBuy();
            this.dateCreate = request.getDateCreate();

        }

        @Override
        public PcInfo build() {
            return new PcInfo(this);
        }
    }

}
