package com.syw.pcroommanager.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcManagementDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcId", nullable = false)
    private PcInfo pcInfo;

    @Column(nullable = false, length = 100)
    private String pcRepairDetail;

    @Column(nullable = false)
    private LocalDateTime dateRepair;

    @Column(nullable = false)
    private Double repairPrice;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;
}
