package com.syw.pcroommanager.entity;

import com.syw.pcroommanager.enums.UseSituation;
import com.syw.pcroommanager.intefaces.CommonModelBuilder;
import com.syw.pcroommanager.model.PcOrderDetailRequest;
import com.syw.pcroommanager.model.PcOrderDetailUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcOrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pcId", nullable = false)
    private PcInfo pcInfo;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length = 15)
    private UseSituation useSituation;

    @Column(nullable = false)
    private Double usePrice;

    @Column(nullable = false)
    private LocalDateTime dateStart;

    @Column(nullable = false)
    private LocalDateTime dateEnd;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putPcOrderDetail (PcOrderDetailUpdateRequest request) {  // 이용내역에 상태
        this.useSituation = request.getUseSituation();

    }

    private PcOrderDetail(PcOrderDetailBuilder builder) {
        this.pcInfo = builder.pcInfo;
        this.useSituation = builder.useSituation;
        this.usePrice = builder.usePrice;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.dateCreate = builder.dateCreate;
    }

    public static class PcOrderDetailBuilder implements CommonModelBuilder<PcOrderDetail> {
        private final PcInfo pcInfo;
        private final UseSituation useSituation;
        private final Double usePrice;
        private final LocalDateTime dateStart;
        private final LocalDateTime dateEnd;
        private final LocalDateTime dateCreate;

        public PcOrderDetailBuilder(PcInfo pcInfo,PcOrderDetailRequest request) {
            this.pcInfo = pcInfo;
            this.useSituation = request.getUseSituation();
            this.usePrice = request.getUsePrice();
            this.dateStart = LocalDateTime.now();
            this.dateEnd = LocalDateTime.now();
            this.dateCreate = LocalDateTime.now();
        }


        @Override
        public PcOrderDetail build() {
            return new PcOrderDetail(this);
        }
    }
}
