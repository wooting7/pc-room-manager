package com.syw.pcroommanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FoodOrderState {
    ORDER("주문"),
    COOK("조리"),
    DELIVERY("배달"),
    COMPLETE("완료");

    private final String name;
}
