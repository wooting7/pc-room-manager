package com.syw.pcroommanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UseSituation {
    USING_START("사용중(시작)"),
    USING_EXTEND("사용중(연장)"),
    NO_USE_COMPLETE("대기중(완료");


    private final String name;
}
