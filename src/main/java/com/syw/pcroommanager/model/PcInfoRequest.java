package com.syw.pcroommanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class PcInfoRequest {

    @ApiModelProperty(notes = "pc이름",required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String pcName;

    @ApiModelProperty(notes = "pc스펙",required = true)
    @NotNull
    @Length(min = 2, max = 50)
    private String pcSpec;

    @ApiModelProperty(notes = "pc가격",required = true)
    @NotNull
    private Double pcPrice;

    @ApiModelProperty(notes = "구입일자",required = true)
    @NotNull
    private LocalDate dateBuy;

    @ApiModelProperty(notes = "생성일",required = true)
    @NotNull
    private LocalDateTime dateCreate;

}
