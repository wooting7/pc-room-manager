package com.syw.pcroommanager.model;

import com.syw.pcroommanager.entity.PcInfo;
import com.syw.pcroommanager.entity.PcOrderDetail;
import com.syw.pcroommanager.enums.UseSituation;
import com.syw.pcroommanager.intefaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PcOrderDetailItem {

    private Long id;
    private PcInfo pcInfo;

    private UseSituation useSituation;

    private Double usePrice;

    private LocalDateTime dateCreate;

    private PcOrderDetailItem(PcOrderDetailItemBuilder builder) {
        this.pcInfo = builder.pcInfo;
        this.useSituation = builder.useSituation;
        this.usePrice = builder.usePrice;
        this.dateCreate = builder.dateCreate;
    }

    public static class PcOrderDetailItemBuilder implements CommonModelBuilder<PcOrderDetailItem> {
        private final PcInfo pcInfo;
        private final UseSituation useSituation;
        private final Double usePrice;
        private final LocalDateTime dateCreate;

        public PcOrderDetailItemBuilder(PcOrderDetail pcOrderDetail) {
            this.pcInfo = pcOrderDetail.getPcInfo();
            this.useSituation = pcOrderDetail.getUseSituation();
            this.usePrice = pcOrderDetail.getUsePrice();
            this.dateCreate = pcOrderDetail.getDateCreate();
        }


        @Override
        public PcOrderDetailItem build() {
            return new PcOrderDetailItem(this);
        }
    }
}
