package com.syw.pcroommanager.model;


import com.syw.pcroommanager.enums.UseSituation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class PcOrderDetailRequest {

    @ApiModelProperty(notes = "pc시퀀스",required = true)
    @NotNull
    private Long pcId;

    @ApiModelProperty(notes = "사용상태",required = true)
    @Enumerated
    private UseSituation useSituation;

    @ApiModelProperty(notes = "이용가격",required = true)
    @NotNull
    private Double usePrice;



}
