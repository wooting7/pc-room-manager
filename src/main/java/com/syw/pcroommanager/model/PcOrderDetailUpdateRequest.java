package com.syw.pcroommanager.model;

import com.syw.pcroommanager.enums.UseSituation;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Getter
@Setter
public class PcOrderDetailUpdateRequest {

    @Enumerated(EnumType.STRING)
    private UseSituation useSituation;


}
