package com.syw.pcroommanager.repository;

import com.syw.pcroommanager.entity.FoodManu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodManuRepository extends JpaRepository<FoodManu, Long> {
}
