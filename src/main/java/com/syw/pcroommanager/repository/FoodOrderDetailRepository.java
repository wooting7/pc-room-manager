package com.syw.pcroommanager.repository;

import com.syw.pcroommanager.entity.FoodOrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodOrderDetailRepository extends JpaRepository<FoodOrderDetail, Long> {
}
