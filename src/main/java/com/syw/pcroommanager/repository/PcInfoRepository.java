package com.syw.pcroommanager.repository;

import com.syw.pcroommanager.entity.PcInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcInfoRepository extends JpaRepository<PcInfo, Long> {
}
