package com.syw.pcroommanager.repository;

import com.syw.pcroommanager.entity.PcManagementDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagementDetailRepository extends JpaRepository<PcManagementDetail, Long> {
}
