package com.syw.pcroommanager.repository;

import com.syw.pcroommanager.entity.PcOrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface PcOrderDetailRepository extends JpaRepository<PcOrderDetail, Long> {
    List<PcOrderDetail> findAllByDateCreateGreaterThanEqualAndDateCreateLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);

}

