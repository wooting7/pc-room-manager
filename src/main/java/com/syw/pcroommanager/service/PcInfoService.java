package com.syw.pcroommanager.service;

import com.syw.pcroommanager.entity.PcInfo;
import com.syw.pcroommanager.entity.PcOrderDetail;
import com.syw.pcroommanager.exception.CMissingDataException;
import com.syw.pcroommanager.model.*;
import com.syw.pcroommanager.repository.PcInfoRepository;
import com.syw.pcroommanager.repository.PcManagementDetailRepository;
import com.syw.pcroommanager.repository.PcOrderDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcInfoService {
    private final PcInfoRepository pcInfoRepository;

    private final PcOrderDetailRepository pcOrderDetailRepository;

    private final PcManagementDetailRepository pcManagementDetailRepository;

    /**
     * pc정보 등록
     * @param request pc 기본정보
     */
    public void setPcInfo (PcInfoRequest request) {
        PcInfo pcInfo = new PcInfo.PcInfoBuilder(request).build();
        pcInfoRepository.save(pcInfo);
    }

    /**
     * pc 시퀀스 가져오는 메서드
     * @param id
     * @return pc 시퀀스
     */
    public PcInfo getPnInfoNumber (long id) {
        return pcInfoRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * pc 이용내역 등록
     * @param pcInfo pc정보
     * @param request pc이용내역
     */
    public void setPcOrderDetail (PcInfo pcInfo, PcOrderDetailRequest request) {
        PcOrderDetail pcOrderDetail = new PcOrderDetail.PcOrderDetailBuilder(pcInfo, request).build();
        pcOrderDetailRepository.save(pcOrderDetail);
    }

    /**
     * pc 이용내역 조회
     * @return pc이용내역 리스트
     */
    public ListResult<PcOrderDetailItem> getPcOrderDetails () {
        List<PcOrderDetail> pcOrderDetails = pcOrderDetailRepository.findAll();

        List<PcOrderDetailItem> result = new LinkedList<>();

        pcOrderDetails.forEach(pcOrderDetail -> {
            PcOrderDetailItem addItem = new PcOrderDetailItem.PcOrderDetailItemBuilder(pcOrderDetail).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }


    /**
     * pc 이용내역 수정하기
     * @param id pc 시퀀스
     * @param updateRequest pc사용상태
     */
    public void putPcOrderDetail (long id, PcOrderDetailUpdateRequest updateRequest) {
        PcOrderDetail pcOrderDetail = pcOrderDetailRepository.findById(id).orElseThrow(CMissingDataException::new);
        pcOrderDetail.putPcOrderDetail(updateRequest);
            }
}
